package com.example.sanket.persistentkvstore;

import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import com.example.sanket.persistentkvstore.activities.MainActivity;
import com.example.sanket.persistentkvstore.database.DatabaseManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.fakes.RoboMenuItem;
import org.robolectric.shadows.ShadowToast;

import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, manifest = "src/main/AndroidManifest.xml")
public class MainActivityTest {

    private MainActivity mActivity;
    private DatabaseManager mDatabaseManager;

    @Before
    public void setup()  {
        mActivity = Robolectric.buildActivity(MainActivity.class)
                .create().get();

        mDatabaseManager = DatabaseManager.getInstance();
    }

    @After
    public void tearDown() {
        mDatabaseManager.close();
    }

    @Test
    public void checkActivityNotNull() throws Exception {
        assertNotNull(mActivity);
    }

    @Test
    public void runMultithread() throws  Exception {
        MenuItem menuItem = new RoboMenuItem(R.id.menu_multithread);
        mActivity.onOptionsItemSelected(menuItem);
        assertThat(ShadowToast.shownToastCount(), equalTo(3));
    }

    @Test
    public void performInsert() throws Exception {
        EditText etKey = (EditText) mActivity.findViewById(R.id.et_key);
        EditText etValue = (EditText) mActivity.findViewById(R.id.et_value);
        Button btnInsert = (Button) mActivity.findViewById(R.id.btn_insert);
        assert etKey != null;
        etKey.setText("asfa");
        assert etValue != null;
        etValue.setText("asfaf");
        assert btnInsert != null;
        btnInsert.performClick();
        assertThat(ShadowToast.getTextOfLatestToast(), equalTo("Insert successful"));
    }
}