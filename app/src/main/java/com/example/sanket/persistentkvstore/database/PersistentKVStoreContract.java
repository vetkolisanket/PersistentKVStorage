package com.example.sanket.persistentkvstore.database;

/**
 * Created by sanket on 4/16/2016.
 */
public final class PersistentKVStoreContract {

    private PersistentKVStoreContract() {
    }

    public static final String DB_NAME = "PersistentKVStore.db";

    public static final int DB_VERSION = 1;

    public static class KeyValue {
        public static final String TABLE_NAME = "keyValue";

        public static final String COLUMN_KEY = "key";

        public static final String COLUMN_VALUE = "value";
    }
}
