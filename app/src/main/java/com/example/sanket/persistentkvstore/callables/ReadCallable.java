package com.example.sanket.persistentkvstore.callables;

import android.database.Cursor;
import android.util.Log;

import com.example.sanket.persistentkvstore.database.DatabaseManager;

import java.util.concurrent.Callable;

/**
 * Created by sanket on 4/16/2016.
 */
public class ReadCallable implements Callable<Boolean> {
    @Override
    public Boolean call() throws Exception {

        Log.d("Read start", "call: read" + System.currentTimeMillis());
        DatabaseManager databaseManager = DatabaseManager.getInstance();
        Cursor cursor = databaseManager.getKeyValue();
        cursor.close();

        Log.d("Read end", "call: read" + System.currentTimeMillis());
        return true;
    }
}
