package com.example.sanket.persistentkvstore.callables;

import android.database.Cursor;
import android.util.Log;

import com.example.sanket.persistentkvstore.activities.MainActivity;
import com.example.sanket.persistentkvstore.database.DatabaseManager;
import com.example.sanket.persistentkvstore.models.KeyValue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;

/**
 * Created by sanket on 4/16/2016.
 */
public class ReadWriteCallable implements Callable<Boolean> {
    @Override
    public Boolean call() throws Exception {
        Log.d("ReadWrite start", "call: read" + System.currentTimeMillis());

        try {
            List<KeyValue> keyValueList = new ArrayList<>(MainActivity.NO_OF_OPERATIONS);
            for (int i = 0; i < MainActivity.NO_OF_OPERATIONS; i++) {
                KeyValue keyValue = new KeyValue(UUID.randomUUID().toString().substring(0, 5), UUID.randomUUID().toString().substring(0, 5));
                keyValueList.add(keyValue);
            }

            DatabaseManager databaseManager = DatabaseManager.getInstance();
            Cursor cursor = databaseManager.getKeyValue();
            cursor.close();

            return databaseManager.insertKeyValue(keyValueList);
        } finally {
            Log.d("ReadWrite end", "call: read" + System.currentTimeMillis());
        }
    }
}
