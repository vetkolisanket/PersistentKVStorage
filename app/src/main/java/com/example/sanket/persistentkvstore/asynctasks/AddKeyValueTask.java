package com.example.sanket.persistentkvstore.asynctasks;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.sanket.persistentkvstore.database.DatabaseManager;
import com.example.sanket.persistentkvstore.models.KeyValue;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sanket on 4/16/2016.
 */
public class AddKeyValueTask extends AsyncTask<KeyValue, Void, List<KeyValue>> {

    private final WeakReference<Context> mContextWeakReference;

    public AddKeyValueTask(Context context) {
        mContextWeakReference = new WeakReference<>(context);
    }

    @Override
    protected List<KeyValue> doInBackground(KeyValue... params) {
        Context context = mContextWeakReference.get();
        if(context != null) {
            DatabaseManager databaseManager = DatabaseManager.getInstance();
            List<KeyValue> keyValueList = Arrays.asList(params);
            if(params.length == 1) {
                KeyValue keyValue = params[0];
                if (databaseManager.insertKeyValue(keyValue)) {
                    return keyValueList;
                }
            } else if(databaseManager.insertKeyValue(keyValueList)) {
                return keyValueList;
            }
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<KeyValue> result) {
        super.onPostExecute(result);
        Context context = mContextWeakReference.get();
        if(context != null) {
            if (result != null) {
                new FetchKeyValueTask(context).execute();
                Toast.makeText(context, "Insert successful", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Insert not successful!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
