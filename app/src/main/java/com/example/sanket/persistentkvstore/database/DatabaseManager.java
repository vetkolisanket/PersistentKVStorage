package com.example.sanket.persistentkvstore.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import com.example.sanket.persistentkvstore.common.PersistentKVApplication;
import com.example.sanket.persistentkvstore.models.KeyValue;

import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by sanket on 4/16/2016.
 */
public class DatabaseManager extends SQLiteOpenHelper {

    private static DatabaseManager mDatabaseManager = null;

    private ReentrantReadWriteLock mRWLock = new ReentrantReadWriteLock();
    private ReentrantReadWriteLock.ReadLock mReadLock = mRWLock.readLock();
    private ReentrantReadWriteLock.WriteLock mWriteLock = mRWLock.writeLock();

    public static DatabaseManager getInstance() {
        if (mDatabaseManager == null) {
            mDatabaseManager = new DatabaseManager(PersistentKVApplication.APP_CONTEXT,
                    PersistentKVStoreContract.DB_NAME, null, PersistentKVStoreContract.DB_VERSION);
        }
        return mDatabaseManager;
    }

    public DatabaseManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_KEYVALUE = "CREATE TABLE " + PersistentKVStoreContract.KeyValue.TABLE_NAME + " ( " +
                PersistentKVStoreContract.KeyValue.COLUMN_KEY + " TEXT PRIMARY KEY, " +
                PersistentKVStoreContract.KeyValue.COLUMN_VALUE + " TEXT " + " ) ";

        db.execSQL(CREATE_TABLE_KEYVALUE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO: 4/16/2016 - in case of db update.
    }

    public boolean isKeyPresentInDB(String key) {
        mReadLock.lock();

        try {
            String Query = "SELECT * FROM " +
                    PersistentKVStoreContract.KeyValue.TABLE_NAME +
                    " WHERE " + PersistentKVStoreContract.KeyValue.COLUMN_KEY + " = '" + key + "'";
            Cursor cursor = getReadableDatabase().rawQuery(Query, null);
            if (cursor.getCount() <= 0) {
                cursor.close();
                return false;
            }
            cursor.close();
            return true;
        } finally {
            mReadLock.unlock();
        }
    }

    public boolean insertKeyValue(KeyValue keyValue) {
        mWriteLock.lock();

        try {
            if (isKeyPresentInDB(keyValue.key)) {
                return updateData(keyValue);
            }

            String INSERT_INTO_KEYVALUE = "INSERT INTO " + PersistentKVStoreContract.KeyValue.TABLE_NAME + " ( " +
                    PersistentKVStoreContract.KeyValue.COLUMN_KEY + "," +
                    PersistentKVStoreContract.KeyValue.COLUMN_VALUE + " ) VALUES(?,?)";
            SQLiteDatabase database = getReadableDatabase();
            SQLiteStatement statement = database.compileStatement(INSERT_INTO_KEYVALUE);
            statement.bindString(1, keyValue.key);
            statement.bindString(2, keyValue.value);
            boolean result = statement.executeInsert() != -1;
            statement.close();
            return result;
        } finally {
            mWriteLock.unlock();
        }
    }

    public boolean insertKeyValue(List<KeyValue> keyValueList) {
        mWriteLock.lock();

        SQLiteDatabase database = getReadableDatabase();
        try {
            database.beginTransaction();
            String INSERT_INTO_KEYVALUE = "INSERT INTO " + PersistentKVStoreContract.KeyValue.TABLE_NAME + " ( " +
                    PersistentKVStoreContract.KeyValue.COLUMN_KEY + "," +
                    PersistentKVStoreContract.KeyValue.COLUMN_VALUE + " ) VALUES(?,?)";

            String UPDATE_VALUE = "UPDATE " + PersistentKVStoreContract.KeyValue.TABLE_NAME +
                    " SET " + PersistentKVStoreContract.KeyValue.COLUMN_VALUE + " = ? " +
                    " WHERE " + PersistentKVStoreContract.KeyValue.COLUMN_KEY + " = ? " ;


            SQLiteStatement insertStatement = database.compileStatement(INSERT_INTO_KEYVALUE);
            SQLiteStatement updateStatement = database.compileStatement(UPDATE_VALUE);

            int size = keyValueList.size();
            for (int i = 0; i < size; i++) {
                KeyValue keyValue = keyValueList.get(i);
                if(isKeyPresentInDB(keyValue.key)) {
                    updateStatement.clearBindings();
                    updateStatement.bindString(1, keyValue.value);
                    updateStatement.bindString(2, keyValue.key);
                    updateStatement.executeUpdateDelete();
                } else {
                    insertStatement.clearBindings();
                    insertStatement.bindString(1, keyValue.key);
                    insertStatement.bindString(2, keyValue.value);
                    insertStatement.executeInsert();
                }
            }

            database.setTransactionSuccessful();
            return true;

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            database.endTransaction();
            mWriteLock.unlock();
        }
    }

    public Cursor getKeyValue() {
        mReadLock.lock();

        try {
            String SELECT_KEYVALUE = "SELECT * FROM " + PersistentKVStoreContract.KeyValue.TABLE_NAME;
            SQLiteDatabase database = getReadableDatabase();
            return database.rawQuery(SELECT_KEYVALUE, null);
        } finally {
            mReadLock.unlock();
        }
    }

    public boolean updateData(KeyValue keyValue) {
        mWriteLock.lock();

        try {
            String UPDATE_VALUE = "UPDATE " + PersistentKVStoreContract.KeyValue.TABLE_NAME +
                    " SET " + PersistentKVStoreContract.KeyValue.COLUMN_VALUE + " = ? " +
                    " WHERE " + PersistentKVStoreContract.KeyValue.COLUMN_KEY + " = '" + keyValue.key + "'";
            SQLiteDatabase database = getReadableDatabase();
            SQLiteStatement statement = database.compileStatement(UPDATE_VALUE);
            statement.bindString(1, keyValue.value);
            boolean result = statement.executeUpdateDelete() > 0;
            statement.close();
            return result;
        } finally {
            mWriteLock.unlock();
        }
    }
}
