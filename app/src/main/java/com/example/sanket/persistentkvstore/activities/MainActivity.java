package com.example.sanket.persistentkvstore.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.sanket.persistentkvstore.R;
import com.example.sanket.persistentkvstore.adapters.KeyValueAdapter;
import com.example.sanket.persistentkvstore.asynctasks.AddKeyValueTask;
import com.example.sanket.persistentkvstore.asynctasks.FetchKeyValueTask;
import com.example.sanket.persistentkvstore.callables.ReadCallable;
import com.example.sanket.persistentkvstore.callables.ReadWriteCallable;
import com.example.sanket.persistentkvstore.callables.WriteCallable;
import com.example.sanket.persistentkvstore.models.KeyValue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class MainActivity extends AppCompatActivity {

    private List<KeyValue> mKeyValueList;

    private KeyValueAdapter mKeyValueAdapter;

    private LinearLayout mLLSingle;
    private LinearLayout mLLMultiple;

    public static final int NO_OF_OPERATIONS = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText etKey = (EditText) findViewById(R.id.et_key);
        final EditText etValue = (EditText) findViewById(R.id.et_value);
        final EditText etKeyOne = (EditText) findViewById(R.id.et_key_one);
        final EditText etValueOne = (EditText) findViewById(R.id.et_value_one);
        final EditText etKeyTwo = (EditText) findViewById(R.id.et_key_two);
        final EditText etValueTwo = (EditText) findViewById(R.id.et_value_two);
        final EditText etKeyThree = (EditText) findViewById(R.id.et_key_three);
        final EditText etValueThree = (EditText) findViewById(R.id.et_value_three);
        Button btnInsert = (Button) findViewById(R.id.btn_insert);
        Button btnInsertAll = (Button) findViewById(R.id.btn_insert_all);
        RecyclerView rvKeyValue = (RecyclerView) findViewById(R.id.rv_key_value_list);
        mLLSingle = (LinearLayout) findViewById(R.id.ll_single);
        mLLMultiple = (LinearLayout) findViewById(R.id.ll_multiple);

        mKeyValueList = new ArrayList<>();

        mKeyValueAdapter = new KeyValueAdapter(mKeyValueList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        if (rvKeyValue != null) {
            rvKeyValue.setLayoutManager(layoutManager);
            rvKeyValue.setAdapter(mKeyValueAdapter);
        }

        if (btnInsert != null) {
            btnInsert.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (etKey != null) {
                        if(TextUtils.isEmpty(etKey.getText().toString())) {
                            etKey.setError("Key cannot be empty!");
                        } else {
                            String key = etKey.getText().toString();
                            String value = null;
                            if (etValue != null) {
                                value = TextUtils.isEmpty(etValue.getText().toString()) ? "" : etValue.getText().toString();
                            }
                            KeyValue keyValue = new KeyValue(key, value);
                            new AddKeyValueTask(MainActivity.this).execute(keyValue);
                        }
                    }
                }
            });
        }

        if (btnInsertAll != null) {
            btnInsertAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (etKeyOne != null && etKeyTwo != null && etKeyThree != null) {
                        if(TextUtils.isEmpty(etKeyOne.getText().toString())
                                && TextUtils.isEmpty(etKeyTwo.getText().toString())
                                && TextUtils.isEmpty(etKeyThree.getText().toString())) {
                            Toast.makeText(MainActivity.this, "Enter value for atleast one key!", Toast.LENGTH_SHORT).show();
                        } else {
                            List<KeyValue> keyValueList = new ArrayList<>();
                            if(!TextUtils.isEmpty(etKeyOne.getText().toString())) {
                                String key = etKeyOne.getText().toString();
                                String value = null;
                                if (etValueOne != null) {
                                    value = TextUtils.isEmpty(etValueOne.getText().toString()) ? "" : etValueOne.getText().toString();
                                }
                                KeyValue keyValue = new KeyValue(key, value);
                                keyValueList.add(keyValue);
                            }
                            if(!TextUtils.isEmpty(etKeyTwo.getText().toString())) {
                                String key = etKeyTwo.getText().toString();
                                String value = null;
                                if (etValueTwo != null) {
                                    value = TextUtils.isEmpty(etValueTwo.getText().toString()) ? "" : etValueTwo.getText().toString();
                                }
                                KeyValue keyValue = new KeyValue(key, value);
                                keyValueList.add(keyValue);
                            }
                            if(!TextUtils.isEmpty(etKeyThree.getText().toString())) {
                                String key = etKeyThree.getText().toString();
                                String value = null;
                                if (etValueThree != null) {
                                    value = TextUtils.isEmpty(etValueThree.getText().toString()) ? "" : etValueThree.getText().toString();
                                }
                                KeyValue keyValue = new KeyValue(key, value);
                                keyValueList.add(keyValue);
                            }
                            KeyValue [] keyValues = new KeyValue[keyValueList.size()];
                            new AddKeyValueTask(MainActivity.this).execute(keyValueList.toArray(keyValues));
                        }
                    }
                }
            });
        }

        if (etKey != null) {
            etKey.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(etKey.getError() != null) {
                        etKey.setError(null);
                    }
                    return false;
                }
            });
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.menu_show) {
            new FetchKeyValueTask(this).execute();
        } else if(item.getItemId() == R.id.menu_multiple) {
            if(item.getTitle().toString().equals(getString(R.string.multiple))) {
                mLLSingle.setVisibility(View.GONE);
                mLLMultiple.setVisibility(View.VISIBLE);
                item.setTitle(R.string.single);
            } else {
                mLLMultiple.setVisibility(View.GONE);
                mLLSingle.setVisibility(View.VISIBLE);
                item.setTitle(R.string.multiple);
            }
        } else if(item.getItemId() == R.id.menu_multithread) {
            performMultiThreading();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Function which runs three separate tasks readDB, writeDB and readWriteDB in parallel
     */
    public void performMultiThreading() {
        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(3);
        Future<Boolean> isReadSuccessful = threadPoolExecutor.submit(new ReadCallable());
        Future<Boolean> isReadWriteSuccessful = threadPoolExecutor.submit(new ReadWriteCallable());
        Future<Boolean> isWriteSuccessful = threadPoolExecutor.submit(new WriteCallable());

        try {
            if(isReadSuccessful.get()) {
                Toast.makeText(MainActivity.this, "Read finished successfully", Toast.LENGTH_SHORT).show();
            }
            if(isReadWriteSuccessful.get()) {
                Toast.makeText(MainActivity.this, "Read-Write finished successfully", Toast.LENGTH_SHORT).show();
            }
            if(isWriteSuccessful.get()) {
                Toast.makeText(MainActivity.this, "Write finished successfully", Toast.LENGTH_SHORT).show();
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    /*
    * Function to update the displayed list
    * */
    public void updateList(List<KeyValue> keyValueList) {
        mKeyValueList.clear();
        mKeyValueList.addAll(keyValueList);
        mKeyValueAdapter.notifyDataSetChanged();
    }
}
