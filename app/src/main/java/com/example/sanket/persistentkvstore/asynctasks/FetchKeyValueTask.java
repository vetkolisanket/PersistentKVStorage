package com.example.sanket.persistentkvstore.asynctasks;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import com.example.sanket.persistentkvstore.activities.MainActivity;
import com.example.sanket.persistentkvstore.database.DatabaseManager;
import com.example.sanket.persistentkvstore.database.PersistentKVStoreContract;
import com.example.sanket.persistentkvstore.models.KeyValue;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sanket on 4/16/2016.
 */
public class FetchKeyValueTask extends AsyncTask<Void, Void, List<KeyValue>> {

    private final WeakReference<Context> mContextWeakReference;

    public FetchKeyValueTask(Context context) {
        mContextWeakReference = new WeakReference<>(context);
    }

    @Override
    protected List<KeyValue> doInBackground(Void... params) {
        Context context = mContextWeakReference.get();
        List<KeyValue> keyValueList = new ArrayList<>();
        if(context != null) {
            DatabaseManager databaseManager = DatabaseManager.getInstance();
            Cursor cursor = databaseManager.getKeyValue();
            while (cursor.moveToNext()) {
                String key = cursor.getString(cursor.getColumnIndex(PersistentKVStoreContract.KeyValue.COLUMN_KEY));
                String value = cursor.getString(cursor.getColumnIndex(PersistentKVStoreContract.KeyValue.COLUMN_VALUE));
                KeyValue keyValue = new KeyValue(key, value);
                keyValueList.add(keyValue);
            }
            cursor.close();
        }
        return keyValueList;
    }

    @Override
    protected void onPostExecute(List<KeyValue> keyValues) {
        super.onPostExecute(keyValues);

        Context context = mContextWeakReference.get();
        if (context != null) {
            ((MainActivity)context).updateList(keyValues);
        }
    }
}
