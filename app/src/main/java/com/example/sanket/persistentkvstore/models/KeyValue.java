package com.example.sanket.persistentkvstore.models;

/**
 * Created by sanket on 4/16/2016.
 */
public class KeyValue {

    public String key;

    public String value;

    public KeyValue(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
