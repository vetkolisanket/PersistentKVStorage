package com.example.sanket.persistentkvstore.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sanket.persistentkvstore.R;
import com.example.sanket.persistentkvstore.models.KeyValue;

import java.util.List;

/**
 * Created by sanket on 4/16/2016.
 */
public class KeyValueAdapter extends RecyclerView.Adapter<KeyValueAdapter.ViewHolder> {

    private List<KeyValue> mKeyValueList;

    public KeyValueAdapter(List<KeyValue> keyValueList) {
        mKeyValueList = keyValueList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_main, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        KeyValue keyValue = mKeyValueList.get(position);

        holder.tvKey.setText(keyValue.key);
        holder.tvValue.setText(keyValue.value);
    }

    @Override
    public int getItemCount() {
        return mKeyValueList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvKey;
        TextView tvValue;

        public ViewHolder(View itemView) {
            super(itemView);
            tvKey = (TextView) itemView.findViewById(R.id.tv_key);
            tvValue = (TextView) itemView.findViewById(R.id.tv_value);
        }
    }
}
