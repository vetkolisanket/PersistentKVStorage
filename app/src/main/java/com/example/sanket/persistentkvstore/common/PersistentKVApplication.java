package com.example.sanket.persistentkvstore.common;

import android.app.Application;
import android.content.Context;

/**
 * Created by sanket on 4/16/2016.
 */
public class PersistentKVApplication extends Application {

    public static Context APP_CONTEXT;

    @Override
    public void onCreate() {
        super.onCreate();
        APP_CONTEXT = this;
    }
}
